from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list(request):
    todos = TodoList.objects.all()
    context = {"todo_list": todos}

    return render(request, "todos/list.html", context)


def todo_items(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo,
    }
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("home_page")
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def edit_todo(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_items", id=id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "todo_list": todo_list,
        "form": form,
    }

    return render(request, "todos/edit.html", context)


def delete_todo(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("home_page")

    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_items", id=item.list.id)

    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/create_item.html", context)


def edit_item(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    print("Item Id: ", todo_item.id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_items", id=todo_item.id)

    else:
        form = TodoItemForm(instance=todo_item)

    context = {"form": form, "todo_item": todo_item}
    return render(request, "todos/edit_item.html", context)
