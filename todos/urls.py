from django.urls import path
from todos.views import (
    todo_list,
    todo_items,
    create_todo,
    edit_todo,
    delete_todo,
    create_item,
    edit_item,
)

urlpatterns = [
    path("", todo_list, name="home_page"),
    path("<int:id>/", todo_items, name="todo_items"),
    path("create/", create_todo, name="create_todo"),
    path("<int:id>/edit.html", edit_todo, name="edit_todo"),
    path("<int:id>/delete.html", delete_todo, name="delete_todo"),
    path("create_item.html/", create_item, name="create_item"),
    path("items/<int:id>/edit/", edit_item, name="edit_item"),
]
